CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers

INTRODUCTION
------------

 Provides deactivation date for user accounts with cron integration.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/user_deactivation_date

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/user_deactivation_date

RECOMMENDED MODULES
-------------------

 * Field permissions (https://www.drupal.org/project/field_permissions) module to control access (edit/view) of the field, both in forms and view displays.

INSTALLATION
------------

 * Install as usual, see
   https://www.drupal.org/docs/8/extending-drupal-8/installing-contributed-modules-find-import-enable-configure-drupal-8 for further
   information.

CONFIGURATION
-------------

 * Make the "Deactivation date" visible in the user account entity form display.
 * To show the configured date in the user profile make the field visible in the user account view display.

TROUBLESHOOTING
---------------

 * The module doesn't provide any visible functions to the user on its own. It provides a base field for the date
    to be stored and a hook_cron() implementation to ensure user accounts are set to inactive.


MAINTAINERS
-----------

Current maintainers:

 * Adam NAGY (https://www.drupal.org/u/joevagyok)
