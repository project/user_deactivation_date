<?php

declare(strict_types=1);

namespace Drupal\Tests\user_deactivation_date\Kernel;

use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\user\Entity\User;

/**
 * Tests the user deactivation cron.
 *
 * @group user_deactivation_date
 */
class UserDeactivationDateTest extends KernelTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = ['datetime', 'user', 'user_deactivation_date'];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installEntitySchema('user');
  }

  /**
   * Test the user deactivation cron.
   */
  public function testDeactivationDateBlocking() {
    $time = $this->container->get('datetime.time')->getRequestTime();
    $time_formatted = date(DateTimeItemInterface::DATE_STORAGE_FORMAT, $time);
    $user_storage = $this->container->get('entity_type.manager')->getStorage('user');

    // Create more than 50 users with a deactivation date in the past.
    for ($i = 0; $i < 55; $i++) {
      $account = User::create([
        'name' => 'user_' . $i,
        'user_deactivation_date' => date(DateTimeItemInterface::DATE_STORAGE_FORMAT, $time - 86400),
        'status' => 1,
      ]);
      $account->save();
    }

    // Create 5 users with a deactivation date in the future.
    for ($i = 55; $i < 60; $i++) {
      $account = User::create([
        'name' => 'user_' . $i,
        'user_deactivation_date' => date(DateTimeItemInterface::DATE_STORAGE_FORMAT, $time + 86400),
        'status' => 1,
      ]);
      $account->save();
    }

    // Run cron.
    $this->container->get('cron')->run();

    // Query for blocked users with a deactivation date in the past.
    $blocked_user_query = $user_storage->getQuery()
      ->condition('user_deactivation_date', $time_formatted, '<=')
      ->condition('status', 0)
      ->accessCheck(FALSE);
    $blocked_users_count = $blocked_user_query->count()->execute();

    // Assert that exactly 50 users with a deactivation date in the past have
    // been blocked.
    $this->assertEquals(50, $blocked_users_count);

    // Run cron again.
    $this->container->get('cron')->run();

    // Update the count for blocked users with a deactivation date in the past.
    $blocked_users_count = $blocked_user_query->count()->execute();

    // Assert that all users with a deactivation date in the past have been
    // blocked now.
    $this->assertEquals(55, $blocked_users_count);

    // Query for active users with a deactivation date in the future.
    $active_future_date_user_query = $user_storage->getQuery()
      ->condition('user_deactivation_date', $time_formatted, '>')
      ->condition('status', 1)
      ->accessCheck(FALSE);
    $active_future_date_users_count = $active_future_date_user_query->count()->execute();

    // Assert that users with a deactivation date in the future are still
    // active.
    $this->assertEquals(5, $active_future_date_users_count);
  }

}
